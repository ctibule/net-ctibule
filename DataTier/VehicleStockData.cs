﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//added
using System.Data;

namespace DataTier
{
    public class VehicleStockData : Data
    {
        /// <summary>
        /// Constructs an instance of VehicleStockData object
        /// </summary>
        /// <param name="connectionString">Defines the connection to the Database</param>
        /// <param name="tableName">Defines which table will be worked on</param>
        /// <param name="selectQuery">Defines which columns will be worked on</param>
        public VehicleStockData(string connectionString, string tableName, string selectQuery) : base(connectionString, tableName, selectQuery) { }

        /// <summary>
        /// Checks if the stock number being checked has a duplicate in the database.
        /// </summary>
        /// <param name="stockNumber">The stock number being checked</param>
        /// <returns>True if there's duplicate, false if not.</returns>
        public bool IsDuplicateStockNumber(string stockNumber)
        {
            bool isThereDuplicate = false;
            DataTable table = base.GetAllRows();

            foreach(DataRow row in table.Rows)
            {
                if(stockNumber.Equals(row["StockNumber"].ToString()))
                {
                    isThereDuplicate = true;
                }
            }

            return isThereDuplicate;
        }
    }
}
