﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//added
using System.Data;
using System.Data.OleDb;

namespace DataTier
{
    /// <summary>
    /// An object that will be used to represent a Database 
    /// </summary>
    public class Data
    {
        private OleDbConnection _connection;
        private OleDbDataAdapter _adapter;
        private OleDbCommandBuilder _commandBuilder;
        private DataSet _dataSet;
        private string _connectionString;
        private string _tableName;
        private string _selectQuery;

        /// <summary>
        /// Constructs an instance of Data object
        /// </summary>
        /// <param name="connectionString">Defines the connection to the Database</param>
        /// <param name="tableName">Defines the table name where the data will be drawn from</param>
        /// <param name="selectQuery"></param>
        public Data(string connectionString, string tableName, string selectQuery)
        {
            //Set the connectionString, tableName and selectQuery
            _connectionString = connectionString;
            _tableName = tableName;
            _selectQuery = selectQuery;

            //Open the connection to the database
            _connection = new OleDbConnection();
            _connection.ConnectionString = _connectionString;
            _connection.Open();

            //Set the SELECT command
            OleDbCommand select = new OleDbCommand();
            select.CommandText = _selectQuery;
            select.Connection = _connection;

            //Set the database connection
            _adapter = new OleDbDataAdapter();
            _adapter.SelectCommand = select;

            //Set the command builder
            _commandBuilder = new OleDbCommandBuilder();
            _commandBuilder.DataAdapter = _adapter;

            //Fill the DataSet with Data from Database
            _dataSet = new DataSet();
            _adapter.Fill(_dataSet, _tableName);

            //EventListener for RowsUpdated event 
            _adapter.RowUpdated += _adapter_RowUpdated;
        }

        /// <summary>
        /// EventHandler for RowsUpdated event of the DataAdapter
        /// </summary>
        private void _adapter_RowUpdated(object sender, OleDbRowUpdatedEventArgs e)
        {
            if(e.StatementType == StatementType.Insert)
            {
                OleDbCommand cmd = new OleDbCommand("SELECT @@IDENTITY", _connection);
                e.Row["ID"] = (int)cmd.ExecuteScalar();
            }
        }

        /// <summary>
        /// Returns a table from the DataSet
        /// </summary>
        /// <returns>A DataSet table</returns>
        public DataTable GetAllRows()
        {
            return _dataSet.Tables[_tableName];
        }

        /// <summary>
        /// Updates the Database
        /// </summary>
        public void Update()
        {
            _adapter.Update(_dataSet.Tables[_tableName]);
        }

        /// <summary>
        /// Closes connection to the database.
        /// </summary>
        public void Close()
        {
            _connection.Close();
        }
    }
}
