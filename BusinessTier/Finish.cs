﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessTier
{
    /// <summary>
    /// Contains fixed price for each type of Finish
    /// </summary>
    public static class Finish
    {
        public static decimal NONE = 0;
        public static decimal STANDARD = 0;
        public static decimal PEARLIZED = 404.04M;
        public static decimal CUSTOM = 606.06M;
    }
}
