﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessTier
{
    public class SalesQuote
    {
        private decimal _vehicleSalePrice;
        private decimal _tradeInValue;
        private decimal _salesTaxRate;
        private Accessories _accessoriesChosen;
        private ExteriorFinish _exteriorFinishChosen;

        /// <summary>
        /// Determines the accessory cost based on chosen accessories.
        /// </summary>
        public decimal AccessoryCost
        {
            get
            {
                decimal accessoryCost;

                //Determine the type of accessory chosen and set the price accordingly.
                switch (_accessoriesChosen)
                {
                    case Accessories.StereoSystem:
                        accessoryCost = Accessory.STEREO_SYSTEM;
                        break;
                    case Accessories.LeatherInterior:
                        accessoryCost = Accessory.LEATHER_INTERIOR;
                        break;
                    case Accessories.StereoAndLeather:
                        accessoryCost = Accessory.STEREO_SYSTEM + Accessory.LEATHER_INTERIOR;
                        break;
                    case Accessories.ComputerNavigation:
                        accessoryCost = Accessory.COMPUTER_NAVIGATION;
                        break;
                    case Accessories.StereoAndNavigation:
                        accessoryCost = Accessory.STEREO_SYSTEM + Accessory.COMPUTER_NAVIGATION;
                        break;
                    case Accessories.LeatherAndNavigation:
                        accessoryCost = Accessory.LEATHER_INTERIOR + Accessory.COMPUTER_NAVIGATION;
                        break;
                    case Accessories.All:
                        accessoryCost = Accessory.STEREO_SYSTEM + Accessory.LEATHER_INTERIOR + Accessory.COMPUTER_NAVIGATION;
                        break;
                    default:
                        accessoryCost = Accessory.NONE;
                        break;
                }

                return accessoryCost;
            }
        }

        /// <summary>
        /// Determines the cost of the Exterior Finish by its type.
        /// </summary>
        public decimal FinishCost
        {
            get
            {
                decimal finishCost;

                //Determine the Exterior Finish type and set the finish cost accordingly.
                switch (_exteriorFinishChosen)
                {
                    case ExteriorFinish.Standard:
                        finishCost = Finish.STANDARD;
                        break;
                    case ExteriorFinish.Pearlized:
                        finishCost = Finish.PEARLIZED;
                        break;
                    case ExteriorFinish.Custom:
                        finishCost = Finish.CUSTOM;
                        break;
                    default:
                        finishCost = Finish.NONE;
                        break;
                }

                return finishCost;
            }
        }

        /// <summary>
        /// Determines the Subtotal of the SalesQuote
        /// </summary>
        public decimal Subtotal
        {
            //Returns the sum of vehicle sale price, accessory cost and finish cost.
            get { return _vehicleSalePrice + AccessoryCost + FinishCost; }
        }

        /// <summary>
        /// Determines the salex tax charged on the Subtotal
        /// </summary>
        public decimal SalesTax
        {
            get { return Subtotal * _salesTaxRate; }
        }

        /// <summary>
        /// Determines the amount after the SalesTax has been charged on Subtotal.
        /// </summary>
        public decimal Total
        {
            get { return Subtotal + SalesTax; }
        }

        /// <summary>
        /// Determines the amount after trade-in value has been deducted from the total amount.
        /// </summary>
        public decimal AmountDue
        {
            get { return Total - _tradeInValue; }
        }

        /// <summary>
        /// Constructs a new instance of SalesQuote object
        /// </summary>
        /// <param name="vehicleSalePrice">The amount of the vehicle</param>
        /// <param name="tradeInValue">The amount of the trade-in value</param>
        /// <param name="salesTaxRate">The sales tax rate that will be changed on the Subtotal</param>
        /// <param name="accessoryChosen">Accessories chosen with the vehicle</param>
        /// <param name="exteriorFinishChosen">Exterior Finish chosen with the vehicle</param>
        public SalesQuote(decimal vehicleSalePrice, decimal tradeInValue, decimal salesTaxRate, Accessories accessoriesChosen, ExteriorFinish exteriorFinishChosen)
        {
            _vehicleSalePrice = vehicleSalePrice;
            _tradeInValue = tradeInValue;
            _salesTaxRate = salesTaxRate;
            _accessoriesChosen = accessoriesChosen;
            _exteriorFinishChosen = exteriorFinishChosen;
        }
    }
}
