﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessTier
{
    /// <summary>
    /// A class that models a ServiceInvoice object
    /// </summary>
    public class ServiceInvoice : Invoice
    {
        /// <summary>
        /// Gets or sets the value of labour cost.
        /// </summary>
        public decimal LabourCost
        {
            get; private set;
        }

        /// <summary>
        /// Gets or sets the value of parts cost.
        /// </summary>
        public decimal PartsCost
        {
            get; private set;
        }

        /// <summary>
        /// Gets or sets the value of material cost.
        /// </summary>
        public decimal MaterialCost
        {
            get; private set;
        }

        /// <summary>
        /// A read-only property that returns the amount of PST being charged on the SUbtotal amount of the the ServiceInvoice.
        /// </summary>
        public override decimal PSTCharged
        {
            get { return (Subtotal - LabourCost) * base.PSTRate; }
        }

        /// <summary>
        /// A read-only property that returns the amount of GST being charged on the Subtotal amount of the ServiceInvoice.
        /// </summary>
        public override decimal GSTCharged
        {
            get { return Subtotal * base.GSTRate; }
        }

        /// <summary>
        /// A read-only property that returns the sum of all costs that can be associated with ServiceInvoice.
        /// </summary>
        public override decimal Subtotal
        {
            get { return LabourCost + PartsCost + MaterialCost; }
        }

        /// <summary>
        /// A read-only property that returns the sum of Subtotal and sales tax charged to the Subtotal.
        /// </summary>
        public override decimal Total
        {
            get { return Subtotal + PSTCharged + GSTCharged; }
        }

        /// <summary>
        /// Constructs an instance of ServiceInvoice by invoking the constructor of its base class.
        /// </summary>
        /// <param name="gstRate">GST rate being charged on the Subtotal amount of the ServiceInvoice.</param>
        /// <param name="pstRate">PST rate being charged on the Subtotal amount of the ServiceInvoice.</param>
        public ServiceInvoice(decimal gstRate, decimal pstRate) : base(gstRate, pstRate) { }

        /// <summary>
        /// Adds cost to the appropriate property of ServiceInvoice object
        /// </summary>
        /// <param name="costType">Identifies which property the amount will be added to.</param>
        /// <param name="amount">The amount of the cost that will be added to a property of ServiceInvoice.</param>
        public void AddCost(CostType costType, decimal amount)
        {
            switch(costType)
            {
                case CostType.Labour:
                    LabourCost += amount;
                    break;
                case CostType.Material:
                    MaterialCost += amount;
                    break;
                case CostType.Parts:
                    PartsCost += amount;
                    break;
            }
        }

    }
}
