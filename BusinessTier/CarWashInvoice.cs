﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessTier
{
    /// <summary>
    /// A class that models a CarWashInvoice object.
    /// </summary>
    public class CarWashInvoice : Invoice
    {
        /// <summary>
        /// Gets and Sets the value of Package cost of the CarWashInvoice
        /// </summary>
        public decimal PackageCost
        {
            get; set;
        }

        /// <summary>
        /// Gets and Sets the value of Fragrance cost of the CarWashInvoice
        /// </summary>
        public decimal FragranceCost
        {
            get; set;
        }

        /// <summary>
        /// A read-only property that gets the value of the amount of PST charged on the Subtotal.
        /// </summary>
        public override decimal PSTCharged
        {
            get { return this.Subtotal * base.PSTRate; }
        }

        /// <summary>
        /// A read-only property that gets the value of the amount of GST charged on the Subtotal.
        /// </summary>
        public override decimal GSTCharged
        {
            get { return this.Subtotal * base.GSTRate; }
        }

        /// <summary>
        /// A read-only property that gets the sum of the Package cost and Fragrance cost associated with the CarWashInvoice
        /// </summary>
        public override decimal Subtotal
        {
            get { return PackageCost + FragranceCost; }
        }

        /// <summary>
        /// A read-only property that gets the sum of the Subtotal and the amount of sales tax charged on it.
        /// </summary>
        public override decimal Total
        {
            get { return Subtotal + PSTCharged + GSTCharged; }
        }

        /// <summary>
        /// Constructs an instance of CarWashInvoice object that accepts 2 parameters
        /// </summary>
        /// <param name="gstRate">GST rate being charged on the Subtotal amount of the CarWashInvoice</param>
        /// <param name="pstRate">PST rate being charged on the Subtotal amount of the CarWashInvoice</param>
        public CarWashInvoice(decimal gstRate, decimal pstRate) : base(gstRate, pstRate) { }

        /// <summary>
        /// Constructs an instnace of CarWashInvoice object that accepts 4 parameters
        /// </summary>
        /// <param name="gstRate">GST rate being charged on the Subtotal amount of the CarWashInvoice</param>
        /// <param name="pstRate">PST rate being charged on the Subtotal amount of the CarWashInvoice</param>
        /// <param name="packageCost">Package cost associated with the CarWashInvoice</param>
        /// <param name="fragranceCost">Fragrance cost associated with the CarWashInvoice</param>
        public CarWashInvoice(decimal gstRate, decimal pstRate, decimal packageCost, decimal fragranceCost) : base(gstRate, pstRate)
        {
            PackageCost = packageCost;
            FragranceCost = fragranceCost;
        }
    }
}
