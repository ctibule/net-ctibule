﻿namespace BusinessTier
{
    /// <summary>
    /// Contains type of accessories
    /// </summary>
    public enum Accessories
    {
        None,
        StereoSystem,
        LeatherInterior,
        StereoAndLeather,
        ComputerNavigation,
        StereoAndNavigation,
        LeatherAndNavigation,
        All
    }
}