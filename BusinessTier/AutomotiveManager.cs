﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//added
using System.IO;
using System.Configuration;

namespace BusinessTier
{
    /// <summary>
    /// Contains various functions that assists operation of the application
    /// </summary>
    public static class AutomotiveManager
    {
        /// <summary>
        /// This method determines if the text can be parsed into a numeric data type
        /// </summary>
        /// <param name="text">represents the text that is going to be tested.</param>
        /// <returns>A boolean value that represents whether the value can be converted to a numeric data type or not.</returns>
        public static bool IsNumeric(string text)
        {
            decimal trialDigit = 0;

            return Decimal.TryParse(text, out trialDigit);
        }

        /// <summary>
        /// This payment method returns the payment for an annuity based on periodic fixed payments and fixed interest rate.
        /// </summary>
        /// <param name="rate">represents the interest rate</param>
        /// <param name="numberOfPaymentPeriods">represents the number of payments that will be made for the annuity</param>
        /// <param name="presentValue">the present value of the annuity</param>
        /// <param name="futureValue">the future value of the annuity</param>
        /// <param name="type">indicates whether annuity payments will be made at the beginning (1) or the end (0) of the payment period.</param>
        /// <returns></returns>
        public static decimal Payment(decimal rate, decimal numberOfPaymentPeriods, decimal presentValue, decimal futureValue = 0, decimal type = 0)
        {
            return (rate == 0) ? presentValue / numberOfPaymentPeriods : rate * (futureValue + presentValue * (decimal)Math.Pow((double)(1 + rate), (double)numberOfPaymentPeriods)) / (((decimal)Math.Pow((double)(1 + rate), (double)numberOfPaymentPeriods) - 1) * (1 + rate * type));
        }

        /// <summary>
        /// Records errors to a log file
        /// </summary>
        /// <param name="errorMessage">A brief description of the error</param>
        /// <param name="ex">The exception where the stack trace will be pulled from.</param>
        public static void RecordError(string errorMessage, Exception ex)
        {
            string errorLogsName = string.Format("Logs\\{0}{1}.log", ConfigurationManager.AppSettings["LogFilePrefix"], DateTime.Today.ToString("MM/dd/yyyy"));
            FileStream stream = new FileStream(errorLogsName, FileMode.Append);
            StreamWriter writer = new StreamWriter(stream);

            writer.WriteLine(string.Format("Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            writer.WriteLine(string.Format("Time: {0}", DateTime.Now.ToString("hh:mm tt")));
            writer.WriteLine(errorMessage);
            writer.WriteLine("Stack trace: {0}", ex.StackTrace);
            writer.WriteLine("-----------------------------------------");
            writer.Flush();
            writer.Close();
        }
    }
}
