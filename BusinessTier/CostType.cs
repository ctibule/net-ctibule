﻿namespace BusinessTier
{
    /// <summary>
    /// Enumerates all possible cost types associated with ServiceInvoice.
    /// </summary>
    public enum CostType
    {
        Labour,
        Parts,
        Material
    }
}