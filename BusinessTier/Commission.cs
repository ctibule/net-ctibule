﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessTier
{
    /// <summary>
    /// Assists the operation of frmVehicleSale
    /// </summary>
    public static class Commission
    {
        //Rate of commission according to seniority and the rate for options sold.
        const decimal JUNIOR_RATE = 0.02M;
        const decimal SENIOR_RATE = 0.05M;
        const decimal EXECUTIVE_RATE = 0.1M;
        const decimal OPTIONS_COMMISSION_RATE = 0.02M;

        /// <summary>
        /// Calculates the commission of an employee for the sale they made based on their seniority.
        /// </summary>
        /// <param name="startDate">The date the employee was hired.</param>
        /// <param name="purchasePrice">The amount of the vehicle the employee sold</param>
        /// <param name="optionsPrice">The amount of options the employee sold along with the vehicle</param>
        /// <returns></returns>
        public static decimal GetCommission(DateTime startDate, decimal purchasePrice, decimal optionsPrice)
        {
            decimal rateOfCommission = 0M;

            //Determine how many years the employee has been employed and apply the appropriate commission rate.
            if (isWithinYearRange(startDate, 15))
            {
                rateOfCommission = EXECUTIVE_RATE;
            }
            else if (isWithinYearRange(startDate, 10))
            {
                rateOfCommission = SENIOR_RATE;
            }
            else
            {
                rateOfCommission = JUNIOR_RATE;
            }

            return (purchasePrice * rateOfCommission) + (optionsPrice * OPTIONS_COMMISSION_RATE);
        }

        /// <summary>
        /// Checks if the start date is within a specified range of years
        /// </summary>
        /// <param name="startDate">The date the employee started working on the company.</param>
        /// <param name="numberOfYears">The range of years that will determine the rate of commission for the employee.</param>
        /// <returns>True if the start date is within the specified range of years</returns>
        private static bool isWithinYearRange(DateTime startDate, int numberOfYears)
        {
            return DateTime.Today.AddYears(-numberOfYears) > startDate;
        }
    }
}
