﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessTier
{
    /// <summary>
    /// An abstract class that models an Invoice
    /// </summary>
    public abstract class Invoice
    {
        /// <summary>
        /// Gets and/or Sets the value of PST Rate property
        /// </summary>
        protected decimal PSTRate
        {
            get; private set;
        }
        
        /// <summary>
        /// Gets and/or Sets the value of GST Rate property
        /// </summary>
        protected decimal GSTRate
        {
            get; private set;
        }

        /// <summary>
        /// Abstract property that returns the amount of PST charged on the invoice
        /// </summary>
        public abstract decimal PSTCharged { get; }

        /// <summary>
        /// Abstract property that returns the amount of GST charged on the invoice
        /// </summary>
        public abstract decimal GSTCharged { get; }

        /// <summary>
        /// Abstract property that returns the Subtotal amount of the invoice.
        /// </summary>
        public abstract decimal Subtotal { get; }

        /// <summary>
        /// Abstract property that returns the Total amount of the invoice.
        /// </summary>
        public abstract decimal Total { get; }

        /// <summary>
        /// Constructs an instance of Invoice object
        /// </summary>
        /// <param name="gstRate">GST rate that will be charged on the invoice</param>
        /// <param name="PSTRate">PST rate that will be charged on the invoice</param>
        public Invoice(decimal gstRate, decimal pstRate)
        {
            GSTRate = gstRate;
            PSTRate = pstRate;
        }
    }
}
