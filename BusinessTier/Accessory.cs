﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessTier
{
    /// <summary>
    /// Contains fixed prices for each Accessory types
    /// </summary>
    public static class Accessory
    {
        public static decimal NONE = 0M;
        public static decimal STEREO_SYSTEM = 505.05M;
        public static decimal LEATHER_INTERIOR = 1010.10M;
        public static decimal COMPUTER_NAVIGATION = 1515.15M;
    }
}
