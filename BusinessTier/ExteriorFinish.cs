﻿namespace BusinessTier
{
    /// <summary>
    /// Contains types of Exterior Finish
    /// </summary>
    public enum ExteriorFinish
    {
        None,
        Standard,
        Pearlized,
        Custom
    }
}