﻿namespace NETctibule
{
    partial class frmFrame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFrame));
            this.msMainMenu = new System.Windows.Forms.MenuStrip();
            this.mnuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileOpenSalesQuote = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileOpenService = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileOpenCarWash = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuWindowTile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuWindowLayer = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuWindowCascade = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.tsToolBar = new System.Windows.Forms.ToolStrip();
            this.tsiOpen = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsiOpenSalesQuote = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiOpenService = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiOpenCarWash = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsiTile = new System.Windows.Forms.ToolStripButton();
            this.tsiLayer = new System.Windows.Forms.ToolStripButton();
            this.tsiCascade = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsiAbout = new System.Windows.Forms.ToolStripButton();
            this.tsiExit = new System.Windows.Forms.ToolStripButton();
            this.mnuData = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDataVehicleStock = new System.Windows.Forms.ToolStripMenuItem();
            this.msMainMenu.SuspendLayout();
            this.tsToolBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // msMainMenu
            // 
            this.msMainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFile,
            this.mnuWindow,
            this.mnuData,
            this.mnuHelp});
            this.msMainMenu.Location = new System.Drawing.Point(0, 0);
            this.msMainMenu.Name = "msMainMenu";
            this.msMainMenu.Size = new System.Drawing.Size(284, 24);
            this.msMainMenu.TabIndex = 1;
            this.msMainMenu.Text = "menuStrip1";
            // 
            // mnuFile
            // 
            this.mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFileOpen,
            this.toolStripMenuItem1,
            this.mnuFileExit});
            this.mnuFile.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.mnuFile.MergeIndex = 1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Size = new System.Drawing.Size(37, 20);
            this.mnuFile.Text = "&File";
            // 
            // mnuFileOpen
            // 
            this.mnuFileOpen.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFileOpenSalesQuote,
            this.mnuFileOpenService,
            this.mnuFileOpenCarWash});
            this.mnuFileOpen.Image = global::NETctibule.Properties.Resources.open;
            this.mnuFileOpen.Name = "mnuFileOpen";
            this.mnuFileOpen.Size = new System.Drawing.Size(134, 22);
            this.mnuFileOpen.Text = "&Open";
            this.mnuFileOpen.ToolTipText = "open";
            // 
            // mnuFileOpenSalesQuote
            // 
            this.mnuFileOpenSalesQuote.Image = global::NETctibule.Properties.Resources.quote;
            this.mnuFileOpenSalesQuote.Name = "mnuFileOpenSalesQuote";
            this.mnuFileOpenSalesQuote.Size = new System.Drawing.Size(136, 22);
            this.mnuFileOpenSalesQuote.Text = "&Sales Quote";
            this.mnuFileOpenSalesQuote.ToolTipText = "quote";
            // 
            // mnuFileOpenService
            // 
            this.mnuFileOpenService.Image = global::NETctibule.Properties.Resources.service;
            this.mnuFileOpenService.Name = "mnuFileOpenService";
            this.mnuFileOpenService.Size = new System.Drawing.Size(136, 22);
            this.mnuFileOpenService.Text = "Ser&vice";
            // 
            // mnuFileOpenCarWash
            // 
            this.mnuFileOpenCarWash.Image = global::NETctibule.Properties.Resources.carwash;
            this.mnuFileOpenCarWash.Name = "mnuFileOpenCarWash";
            this.mnuFileOpenCarWash.Size = new System.Drawing.Size(136, 22);
            this.mnuFileOpenCarWash.Text = "Car &Wash";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(131, 6);
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Image = global::NETctibule.Properties.Resources.exit;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.mnuFileExit.Size = new System.Drawing.Size(134, 22);
            this.mnuFileExit.Text = "E&xit";
            this.mnuFileExit.ToolTipText = "exit";
            // 
            // mnuWindow
            // 
            this.mnuWindow.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuWindowTile,
            this.mnuWindowLayer,
            this.mnuWindowCascade});
            this.mnuWindow.Name = "mnuWindow";
            this.mnuWindow.Size = new System.Drawing.Size(63, 20);
            this.mnuWindow.Text = "&Window";
            // 
            // mnuWindowTile
            // 
            this.mnuWindowTile.Image = global::NETctibule.Properties.Resources.tile;
            this.mnuWindowTile.Name = "mnuWindowTile";
            this.mnuWindowTile.Size = new System.Drawing.Size(118, 22);
            this.mnuWindowTile.Text = "&Tile";
            this.mnuWindowTile.ToolTipText = "tile";
            // 
            // mnuWindowLayer
            // 
            this.mnuWindowLayer.Image = global::NETctibule.Properties.Resources.layer;
            this.mnuWindowLayer.Name = "mnuWindowLayer";
            this.mnuWindowLayer.Size = new System.Drawing.Size(118, 22);
            this.mnuWindowLayer.Text = "&Layer";
            this.mnuWindowLayer.ToolTipText = "layer";
            // 
            // mnuWindowCascade
            // 
            this.mnuWindowCascade.Image = global::NETctibule.Properties.Resources.cascade;
            this.mnuWindowCascade.Name = "mnuWindowCascade";
            this.mnuWindowCascade.Size = new System.Drawing.Size(118, 22);
            this.mnuWindowCascade.Text = "&Cascade";
            this.mnuWindowCascade.ToolTipText = "cascade";
            // 
            // mnuHelp
            // 
            this.mnuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuHelpAbout});
            this.mnuHelp.Name = "mnuHelp";
            this.mnuHelp.Size = new System.Drawing.Size(44, 20);
            this.mnuHelp.Text = "&Help";
            // 
            // mnuHelpAbout
            // 
            this.mnuHelpAbout.Image = global::NETctibule.Properties.Resources.about;
            this.mnuHelpAbout.Name = "mnuHelpAbout";
            this.mnuHelpAbout.Size = new System.Drawing.Size(107, 22);
            this.mnuHelpAbout.Text = "&About";
            this.mnuHelpAbout.ToolTipText = "about";
            // 
            // tsToolBar
            // 
            this.tsToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsiOpen,
            this.toolStripSeparator1,
            this.tsiTile,
            this.tsiLayer,
            this.tsiCascade,
            this.toolStripSeparator2,
            this.tsiAbout,
            this.tsiExit});
            this.tsToolBar.Location = new System.Drawing.Point(0, 24);
            this.tsToolBar.Name = "tsToolBar";
            this.tsToolBar.Size = new System.Drawing.Size(284, 25);
            this.tsToolBar.TabIndex = 3;
            this.tsToolBar.Text = "toolStrip1";
            // 
            // tsiOpen
            // 
            this.tsiOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsiOpen.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsiOpenSalesQuote,
            this.tsiOpenService,
            this.tsiOpenCarWash});
            this.tsiOpen.Image = global::NETctibule.Properties.Resources.open;
            this.tsiOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsiOpen.Name = "tsiOpen";
            this.tsiOpen.Size = new System.Drawing.Size(29, 22);
            this.tsiOpen.Text = "toolStripDropDownButton1";
            this.tsiOpen.ToolTipText = "open";
            // 
            // tsiOpenSalesQuote
            // 
            this.tsiOpenSalesQuote.Image = global::NETctibule.Properties.Resources.quote;
            this.tsiOpenSalesQuote.Name = "tsiOpenSalesQuote";
            this.tsiOpenSalesQuote.Size = new System.Drawing.Size(136, 22);
            this.tsiOpenSalesQuote.Text = "&Sales Quote";
            this.tsiOpenSalesQuote.ToolTipText = "quote";
            // 
            // tsiOpenService
            // 
            this.tsiOpenService.Image = global::NETctibule.Properties.Resources.service;
            this.tsiOpenService.Name = "tsiOpenService";
            this.tsiOpenService.Size = new System.Drawing.Size(136, 22);
            this.tsiOpenService.Text = "Ser&vice";
            // 
            // tsiOpenCarWash
            // 
            this.tsiOpenCarWash.Image = global::NETctibule.Properties.Resources.carwash1;
            this.tsiOpenCarWash.Name = "tsiOpenCarWash";
            this.tsiOpenCarWash.Size = new System.Drawing.Size(136, 22);
            this.tsiOpenCarWash.Text = "Car &Wash";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsiTile
            // 
            this.tsiTile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsiTile.Image = global::NETctibule.Properties.Resources.tile;
            this.tsiTile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsiTile.Name = "tsiTile";
            this.tsiTile.Size = new System.Drawing.Size(23, 22);
            this.tsiTile.Text = "toolStripButton1";
            this.tsiTile.ToolTipText = "tile";
            // 
            // tsiLayer
            // 
            this.tsiLayer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsiLayer.Image = global::NETctibule.Properties.Resources.layer;
            this.tsiLayer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsiLayer.Name = "tsiLayer";
            this.tsiLayer.Size = new System.Drawing.Size(23, 22);
            this.tsiLayer.Text = "toolStripButton1";
            this.tsiLayer.ToolTipText = "layer";
            // 
            // tsiCascade
            // 
            this.tsiCascade.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsiCascade.Image = global::NETctibule.Properties.Resources.cascade;
            this.tsiCascade.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsiCascade.Name = "tsiCascade";
            this.tsiCascade.Size = new System.Drawing.Size(23, 22);
            this.tsiCascade.Text = "toolStripButton1";
            this.tsiCascade.ToolTipText = "cascade";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tsiAbout
            // 
            this.tsiAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsiAbout.Image = global::NETctibule.Properties.Resources.about;
            this.tsiAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsiAbout.Name = "tsiAbout";
            this.tsiAbout.Size = new System.Drawing.Size(23, 22);
            this.tsiAbout.Text = "toolStripButton1";
            this.tsiAbout.ToolTipText = "about";
            // 
            // tsiExit
            // 
            this.tsiExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsiExit.Image = global::NETctibule.Properties.Resources.exit;
            this.tsiExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsiExit.Name = "tsiExit";
            this.tsiExit.Size = new System.Drawing.Size(23, 22);
            this.tsiExit.Text = "toolStripButton1";
            this.tsiExit.ToolTipText = "exit";
            // 
            // mnuData
            // 
            this.mnuData.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuDataVehicleStock});
            this.mnuData.Name = "mnuData";
            this.mnuData.Size = new System.Drawing.Size(43, 20);
            this.mnuData.Text = "&Data";
            // 
            // mnuDataVehicleStock
            // 
            this.mnuDataVehicleStock.Name = "mnuDataVehicleStock";
            this.mnuDataVehicleStock.Size = new System.Drawing.Size(152, 22);
            this.mnuDataVehicleStock.Text = "&Vehicle Stock";
            // 
            // frmFrame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.tsToolBar);
            this.Controls.Add(this.msMainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.msMainMenu;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFrame";
            this.Text = "Sales Quote";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.msMainMenu.ResumeLayout(false);
            this.msMainMenu.PerformLayout();
            this.tsToolBar.ResumeLayout(false);
            this.tsToolBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msMainMenu;
        private System.Windows.Forms.ToolStripMenuItem mnuFile;
        private System.Windows.Forms.ToolStripMenuItem mnuFileOpen;
        private System.Windows.Forms.ToolStripMenuItem mnuFileOpenSalesQuote;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mnuFileExit;
        private System.Windows.Forms.ToolStripMenuItem mnuWindow;
        private System.Windows.Forms.ToolStripMenuItem mnuWindowTile;
        private System.Windows.Forms.ToolStripMenuItem mnuWindowLayer;
        private System.Windows.Forms.ToolStripMenuItem mnuWindowCascade;
        private System.Windows.Forms.ToolStripMenuItem mnuHelp;
        private System.Windows.Forms.ToolStripMenuItem mnuHelpAbout;
        private System.Windows.Forms.ToolStrip tsToolBar;
        private System.Windows.Forms.ToolStripDropDownButton tsiOpen;
        private System.Windows.Forms.ToolStripMenuItem tsiOpenSalesQuote;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsiTile;
        private System.Windows.Forms.ToolStripButton tsiLayer;
        private System.Windows.Forms.ToolStripButton tsiCascade;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsiAbout;
        private System.Windows.Forms.ToolStripButton tsiExit;
        private System.Windows.Forms.ToolStripMenuItem mnuFileOpenService;
        private System.Windows.Forms.ToolStripMenuItem tsiOpenService;
        private System.Windows.Forms.ToolStripMenuItem mnuFileOpenCarWash;
        private System.Windows.Forms.ToolStripMenuItem tsiOpenCarWash;
        private System.Windows.Forms.ToolStripMenuItem mnuData;
        private System.Windows.Forms.ToolStripMenuItem mnuDataVehicleStock;
    }
}

