﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//added
using BusinessTier;

namespace NETctibule
{
    public partial class frmCarWashInvoice : frmInvoice
    {
        public frmCarWashInvoice(CarWashInvoice invoice)
        {
            InitializeComponent();

            //Set the labels
            lblInvoiceTitle.Text = "Car Wash Invoice";
            lblPackagePrice.Text = invoice.PackageCost.ToString("C");
            lblFragrancePrice.Text = invoice.FragranceCost.ToString("N");
            lblSubtotal.Text = invoice.Subtotal.ToString("C");
            lblTaxes.Text = string.Format("{0:N}", invoice.GSTCharged + invoice.PSTCharged);
            lblTotal.Text = invoice.Total.ToString("C");
        }
    }
}
