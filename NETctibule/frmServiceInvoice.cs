﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//added
using BusinessTier;

namespace NETctibule
{
    public partial class frmServiceInvoice : frmInvoice
    {
        public frmServiceInvoice(ServiceInvoice invoice)
        {
            InitializeComponent();

            //Set the labels of this form
            lblInvoiceTitle.Text = "Service Invoice";
            lblLabourCost.Text = invoice.LabourCost.ToString("C");
            lblPartsCost.Text = invoice.PartsCost.ToString("N");
            lblMaterialsCost.Text = invoice.MaterialCost.ToString("N");
            lblSubtotal.Text = invoice.Subtotal.ToString("C");
            lblPST.Text = invoice.PSTCharged.ToString("N");
            lblGST.Text = invoice.GSTCharged.ToString("N");
            lblTotal.Text = invoice.Total.ToString("C");
        }
    }
}
