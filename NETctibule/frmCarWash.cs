﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//added 
using System.Configuration;
using System.IO;
using BusinessTier;

namespace NETctibule
{
    #region Structs
    public partial class frmCarWash : Form
    {
        /// <summary>
        /// A structure that describes a Fragrance
        /// </summary>
        private struct Fragrance
        {
            public string Description { get; set; }

            public decimal Price { get; set; }
        }

        /// <summary>
        /// A structure that describes a Package
        /// </summary>
        private struct Package
        {
            List<string> _interior;
            List<string> _exterior;

            public string Description { get; set; }

            public decimal Price { get; set; }

            public List<string> Interior
            {
                get { return _interior; }
                set { _interior = value; }
            }
            
            public List<string> Exterior
            {
                get { return _exterior; }
                set { _exterior = value; }
            }

            public void AddToInterior(string interior)
            {
                _interior.Add(interior);
            }

            public void AddToExterior(string exterior)
            {
                _exterior.Add(exterior);
            }
        }
        #endregion

        public frmCarWash()
        {
            InitializeComponent();

            //Event Listener for the load event of this form
            this.Load += FrmCarWash_Load;

            //Event Listener for the ComboBoxes
            ComboBox[] comboboxes = { cboPackage, cboFragrance };

            foreach(ComboBox combobox in comboboxes)
            {
                combobox.SelectedIndexChanged += Combobox_SelectedIndexChanged;
            }

            //Event Listener for mnuCarWashGenerateInvoice 
            mnuGenerateInvoice.Click += MnuGenerateInvoice_Click;
        }

        #region EventHandlers

        /// <summary>
        /// Event Handler for the click event of the Generate Invoice menu item
        /// </summary>
        private void MnuGenerateInvoice_Click(object sender, EventArgs e)
        {
            //Open Car Wash Invoice form
            frmCarWashInvoice carWashInvoice = new frmCarWashInvoice(CalculateCarWashInvoice());
            carWashInvoice.MdiParent = this.MdiParent;
            carWashInvoice.Show();

            //Reset the default value for the combo boxes of this form
            cboPackage.SelectedIndex = 0;
            cboFragrance.SelectedIndex = 2;
        }

        /// <summary>
        /// Event Handler for SelectedIndexChanged of comboboxes
        /// </summary>
        private void Combobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Upate the list of services and the labels to show the running total
            if (cboPackage.SelectedIndex > -1 && cboFragrance.SelectedIndex > -1)
            {
                UpdateListOfServices();
                UpdateInvoiceLabels(CalculateCarWashInvoice());
            }
        }

        /// <summary>
        /// Event Handler for the load event of this form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmCarWash_Load(object sender, EventArgs e)
        {
            try
            {
                //Populate Fragrance ComboBox
                PopulateComboBoxes(new FileStream("Data\\FragranceData.txt", FileMode.Open), cboFragrance);

                //Populate Package ComboBox
                PopulateComboBoxes(new FileStream("Data\\PackageData.txt", FileMode.Open), cboPackage);

                //Set the default Package and Fragrance
                cboPackage.SelectedIndex = 0;
                cboFragrance.SelectedIndex = 2;
            }
            //Catch exceptions when reading Package and Fragrance files
            catch (IOException ex)
            {
                DisplayAndRecordErrors(ex);
            }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Gets data from the text file, construct a struct that represents it and add it to the ComboBox
        /// </summary>
        /// <param name="file">The file where the data will be retrieved from.</param>
        /// <param name="combobox">The ComboBox where the data from the file will be added to.</param>
        private void PopulateComboBoxes(FileStream file, ComboBox comboBox)
        {
            StreamReader reader = new StreamReader(file);
            int recordNum = 0;

            //Loop through the file
            while(reader.Peek() != -1)
            {
                //Read a record from the file and store its field to an array of string
                string[] fields = reader.ReadLine().Split(',');

                //If combobox is cboFragrance, create a Fragrance struct and add it to the combobox
                if(comboBox == cboFragrance)
                {
                    //Construct a struct that represents it as an object
                    Fragrance fragrance = new Fragrance
                    {
                        Description = fields[0],
                        Price = decimal.Parse(fields[1])
                    };

                    //Add it to the ComboBox
                    comboBox.Items.Add(fragrance);
                    comboBox.ValueMember = "Price";
                    comboBox.DisplayMember = "Description";
                }
                //If the ComboBox that will be populated is not cboFragrance, construct a struct that represents a Package.
                else
                {
                    Package package = new Package
                    {
                        Description = fields[0],
                        Price = decimal.Parse(fields[1]),
                        Interior = ReadServicesList(new FileStream("Data\\Interior.txt", FileMode.Open), recordNum),
                        Exterior = ReadServicesList(new FileStream("Data\\Exterior.txt", FileMode.Open), recordNum)
                    };

                    //Add it to the ComboBox
                    comboBox.Items.Add(package);
                    comboBox.ValueMember = "Price";
                    comboBox.DisplayMember = "Description";
                }

                //Increment record number
                recordNum++;
            }

            //Close the reader
            reader.Close();
        }

        /// <summary>
        /// Reads the file where the services are stored and returns a list of services attached to a particular package.
        /// </summary>
        /// <param name="servicesFile"></param>
        /// <param name="numberOfServices"></param>
        private List<string> ReadServicesList(FileStream servicesFile, int numberOfServices)
        {
            List<string> services = new List<string>();
            StreamReader reader = new StreamReader(servicesFile);
            int numberOfLinesRead = -1;

            while(numberOfLinesRead < numberOfServices)
            {
                services.Add(reader.ReadLine());
                numberOfLinesRead++;
            }

            //Close the reader
            reader.Close();

            return services;
        }

        /// <summary>
        /// Updates the list of services associated with the Package/Fragrance
        /// </summary>
        private void UpdateListOfServices()
        {
            //Clear the list of services
            lstInterior.Items.Clear();
            lstExterior.Items.Clear();

            //Define the list of interior and exterior services
            List<string> interiorServices = ((Package)cboPackage.SelectedItem).Interior;
            List<string> exteriorServices = ((Package)cboPackage.SelectedItem).Exterior;

            //Loop through the list and display the services associated with the selected package.
            for (int i = 0; i < interiorServices.Count && i < exteriorServices.Count; i++)
            {
                //Modify the 1st interior service
                if (i == 0)
                {
                    lstInterior.Items.Add(string.Format("{0} - {1}", interiorServices[i], ((Fragrance)cboFragrance.SelectedItem).Description));
                }
                else
                {
                    lstInterior.Items.Add(interiorServices[i]);
                }

                //Display exterior services
                lstExterior.Items.Add(exteriorServices[i]);
            }
        }

        /// <summary>
        /// Calculate the running total of CarWashInvoice
        /// </summary>
        private CarWashInvoice CalculateCarWashInvoice()
        {
            CarWashInvoice invoice = new CarWashInvoice(decimal.Parse(ConfigurationManager.AppSettings["GSTRate"]), decimal.Parse(ConfigurationManager.AppSettings["PSTRate"]));
            invoice.PackageCost = ((Package)cboPackage.SelectedItem).Price;
            invoice.FragranceCost = ((Fragrance)cboFragrance.SelectedItem).Price;

            return invoice;
        }

        /// <summary>
        /// Updates the labels to show details about the CarWashInvoice
        /// </summary>
        /// <param name="invoice">the CarWashInvoice's running totaal</param>
        private void UpdateInvoiceLabels(CarWashInvoice invoice)
        {
            //Update labels
            lblSubtotal.Text = invoice.Subtotal.ToString("C");
            lblPST.Text = invoice.PSTCharged.ToString("N");
            lblGST.Text = invoice.GSTCharged.ToString("N");
            lblTotal.Text = invoice.Total.ToString("C");
        }

        /// <summary>
        /// Display a message box to the screen indicating that an error has been found and record it to the log file.
        /// </summary>
        private void DisplayAndRecordErrors(Exception ex)
        {
            try
            {
                string errorMessage = "An error occured while loading car wash data";
                //Display error message
                MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                //Write to a log file
                string errorLogsName = string.Format("Logs\\{0}{1}.log", ConfigurationManager.AppSettings["LogFilePrefix"], DateTime.Today.ToString("MM/dd/yyyy"));
                FileStream stream = new FileStream(errorLogsName, FileMode.Append);
                StreamWriter writer = new StreamWriter(stream);

                writer.WriteLine(string.Format("Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
                writer.WriteLine(string.Format("Time: {0}", DateTime.Now.ToString("hh:mm tt")));
                writer.WriteLine(errorMessage);
                writer.WriteLine("Stack trace: {0}", ex.StackTrace);
                writer.WriteLine("-----------------------------------------");
                writer.Flush();
                writer.Close();
            }
            //Catch exceptions when attempting to write to logs file and close this form.
            catch (Exception)
            {
                MessageBox.Show("An error occured while writing to the error log file.", "Log File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                this.Shown += delegate (object s, EventArgs e)
                {
                    this.Close();
                };
            }
        }
        #endregion
    }
}
