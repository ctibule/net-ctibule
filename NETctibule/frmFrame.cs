﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//added
using System.Configuration;

namespace NETctibule
{
    public partial class frmFrame : Form
    {
        public frmFrame()
        {
            InitializeComponent();

            //Set the form's title
            this.Text = ConfigurationManager.AppSettings["ApplicationName"];

            //Event Listener for opening SalesQuote form
            mnuFileOpenSalesQuote.Click += OpenSalesQuote_Click;
            tsiOpenSalesQuote.Click += OpenSalesQuote_Click;

            //Event Listener for closing this form
            mnuFileExit.Click += FileExit_Click;
            tsiExit.Click += FileExit_Click;

            //Event Listener for opening About form
            mnuHelpAbout.Click += HelpAbout_Click;
            tsiAbout.Click += HelpAbout_Click;

            //Event Listener for laying out child forms in horizontal layout
            mnuWindowTile.Click += Tile_Click;
            tsiTile.Click += Tile_Click;

            //Event Listener for laying out child forms in vertical layout
            mnuWindowLayer.Click += Layer_Click;
            tsiLayer.Click += Layer_Click;

            //Event Listener for laying out child forms in cascade
            mnuWindowCascade.Click += Cascade_Click;
            tsiCascade.Click += Cascade_Click;

            //Added @ A5 - Event Listener for opening frmService
            mnuFileOpenService.Click += OpenService_Click;
            tsiOpenService.Click += OpenService_Click;

            //Added @ A6 - Event Listener for opening frmCarWash
            mnuFileOpenCarWash.Click += OpenCarWash_Click;
            tsiOpenCarWash.Click += OpenCarWash_Click;

            //Added @ A7 - Event Listener for opening frmVehicleStock
            mnuDataVehicleStock.Click += MnuDataVehicleStock_Click;
        }
        /// <summary>
        /// Handles the click event for any controls used to open Vehicle stock form
        /// </summary>
        private void MnuDataVehicleStock_Click(object sender, EventArgs e)
        {
            bool isVehicleStockFormOpen = false;

            //Check if there's no instance of frmVehicleStock is open
            foreach(Form form in Application.OpenForms)
            {
                if(form is frmVehicleStock)
                {
                    isVehicleStockFormOpen = true;
                }
            }

            //If there's no instance of frmVehicleStockForm open, allow an instance of it to be opened
            if (!isVehicleStockFormOpen)
            {
                frmVehicleStock vehicleStock = new frmVehicleStock();
                vehicleStock.MdiParent = this;
                vehicleStock.Show();
            }

        }

        /// <summary>
        /// Handles the click event for any controls used to open Car Wash form.
        /// </summary>
        private void OpenCarWash_Click(object sender, EventArgs e)
        {
            frmCarWash carWash = new frmCarWash();
            carWash.MdiParent = this;
            carWash.Show();
        }

        /// <summary>
        /// Handles the click event for any controls used to open Service form.
        /// </summary>
        private void OpenService_Click(object sender, EventArgs e)
        {
            frmService service = new frmService();
            service.MdiParent = this;
            service.Show();
        }

        /// <summary>
        /// Handles the click event for any controls used to lay out child forms in cascade
        /// </summary>
        private void Cascade_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(System.Windows.Forms.MdiLayout.Cascade);
        }

        /// <summary>
        /// Handles the click event for any controls used to lay out child forms in vertical layout
        /// </summary>
        private void Layer_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(System.Windows.Forms.MdiLayout.TileVertical);
        }

        /// <summary>
        /// Handles the click event for any controls used to lay out child forms in horizontal layout
        /// </summary>
        private void Tile_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(System.Windows.Forms.MdiLayout.TileHorizontal);
        }

        /// <summary>
        /// Handles the click event for any controls used to open the About form.
        /// </summary>
        private void HelpAbout_Click(object sender, EventArgs e)
        {
            frmAbout about = new frmAbout();
            about.ShowDialog();
        }

        /// <summary>
        /// Handles the click event for any controls used to close this form.
        /// </summary>
        private void FileExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Handles the click event for any controls used to open a Sales Quote form.
        /// </summary>
        private void OpenSalesQuote_Click(object sender, EventArgs e)
        {
            frmQuote quote = new frmQuote();
            quote.MdiParent = this;
            quote.Show();
        }
    }
}
