﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//added
using System.Configuration;
using BusinessTier;

namespace NETctibule
{
    public partial class frmQuote : Form
    {
        public frmQuote()
        {
            InitializeComponent();

            //Event Listener for the load event of this form
            this.Load += FrmQuote_Load;

            //Event Listener for the lnkResetForm
            lnkResetForm.Click += LnkResetForm_Click;

            //Event Listener for btnCalculateQuote
            btnCalculateQuote.Click += BtnCalculateQuote_Click;

            //Validate textboxes
            TextBox[] textboxes = { txtVehicleSalePrice, txtTradeInAllowance };

            foreach (TextBox textbox in textboxes)
            {
                textbox.Validating += Textbox_Validating;
                textbox.Validated += Textbox_Validated;
            }

            //Event Listener for the value changed event of the horizontal scroll bars
            HScrollBar[] horizontalscrollbars = { hsbNumberOfYears, hsbInterestRate };

            foreach (HScrollBar horizontalscrollbar in horizontalscrollbars)
            {
                horizontalscrollbar.ValueChanged += Horizontalscrollbar_ValueChanged;
            }

            //Event Listener for the checked changed event of radio and control buttons
            Control[] radiosAndChecks = { chkStereoSystem, chkLeatherInterior, chkComputerNavigation, radStandard, radPearlized, radCustom };

            foreach (Control control in radiosAndChecks)
            {
                //If control is a radiobutton, cast it as such and add Event Listener for CheckedChanged event.
                if(control is RadioButton)
                {
                    ((RadioButton)control).CheckedChanged += FrmQuote_CheckedChanged;
                }
                //Else, cast control as a CheckBox and add Event Listener for CheckedChanged event.
                else
                {
                    ((CheckBox)control).CheckedChanged += FrmQuote_CheckedChanged;
                }
            }
        }

        #region EventHandlers

        /// <summary>
        /// Handles the value changed event of the horizontal scrollbar
        /// </summary>
        private void Horizontalscrollbar_ValueChanged(object sender, EventArgs e)
        {
            UpdateFinanceGroup(CalculateQuote());
        }

        /// <summary>
        /// Handles the click event of lnkResetForm
        /// </summary>
        private void LnkResetForm_Click(object sender, EventArgs e)
        {
            //Prompt the user to ask if he/she wants to reset the form,
            DialogResult result = MessageBox.Show("Do you want to reset this sales quote?", "Sales Quote", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

            //If the user wants to reset the form, do so.
            if (result == DialogResult.OK)
            {
                SetToDefaultState();
            }
        }

        /// <summary>
        /// Handles the load event of this form
        /// </summary>
        private void FrmQuote_Load(object sender, EventArgs e)
        {
            SetToDefaultState();
        }

        /// <summary>
        /// Handles the checked change event of the radio buttons and checkboxes of this form
        /// </summary>
        private void FrmQuote_CheckedChanged(object sender, EventArgs e)
        {
            if(grpFinance.Enabled)
            {
                //Calculate SalesQuote
                SalesQuote quote = CalculateQuote();

                //Update Summary and Finance groups
                UpdateSummaryLabels(quote);
                UpdateFinanceGroup(quote);
            }
        }

        /// <summary>
        /// Handles the validated event of a textbox control
        /// </summary>
        private void Textbox_Validated(object sender, EventArgs e)
        {
            //Set the error provider to empty
            errorProvider.SetError((TextBox)sender, string.Empty);
        }

        /// <summary>
        /// Handles the validation of textbox controls
        /// </summary>
        private void Textbox_Validating(object sender, CancelEventArgs e)
        {
            string errorMessage = string.Empty;

            //Check if the textbox is empty or if the input is not numeric.
            if (!AutomotiveManager.IsNumeric(((TextBox)sender).Text))
            {
                errorMessage = "Please enter a numeric value.";
            }
            //Check if the object sending the event is txtVehicleSalePrice and if the value is lesser than 1.
            else if (((TextBox)sender) == txtVehicleSalePrice && decimal.Parse(((TextBox)sender).Text) < 1)
            {
                errorMessage = "Please enter a value greater than 0.";
            }
            //Check if the object sending the event is txtTradeInAllowance and if the value is lesser than 0.
            else if (((TextBox)sender) == txtTradeInAllowance && decimal.Parse(((TextBox)sender).Text) < 0)
            {
                errorMessage = "Please enter a value greater than or equal to 0.";
            }

            //If the error message is not empty, show the error provider.
            if (errorMessage.Length != 0)
            {
                e.Cancel = true;
                ((TextBox)sender).SelectAll();
                ((TextBox)sender).Focus();
                errorProvider.SetError((TextBox)sender, errorMessage);
            }
        }

        /// <summary>
        /// Handles the click event for the btnCalculate
        /// </summary>
        private void BtnCalculateQuote_Click(object sender, EventArgs e)
        {
            //Validate controls
            if (this.ValidateChildren())
            {
                //Define and calculate the SalesQuote
                SalesQuote quote = CalculateQuote();

                //Update Summary group
                UpdateSummaryLabels(quote);

                //Enable the Finance group
                grpFinance.Enabled = true;

                //Set default values for the scroll bars
                hsbNumberOfYears.Value = 3;
                hsbInterestRate.Value = 500;

                //Calculate Monthly payment
                UpdateFinanceGroup(quote);
            }
        }
        #endregion

        #region Method

        /// <summary>
        /// Set the form to its default state
        /// </summary>
        private void SetToDefaultState()
        {
            //Set the Summary and Finance groups to its default state
            ClearSummaryAndFinance();

            //Highlight txtVehicleSalePrice
            txtVehicleSalePrice.Focus();

            //Set txtVehicleSalePrice input to empty
            txtVehicleSalePrice.Text = string.Empty;

            //Set txtTradeInAllowance to its default value
            txtTradeInAllowance.Text = "0";

            //Check radStandard
            radStandard.Checked = true;

            //Uncheck Checkboxes
            CheckBox[] checkboxes = { chkStereoSystem, chkLeatherInterior, chkComputerNavigation };

            foreach (CheckBox checkbox in checkboxes)
            {
                checkbox.Checked = false;
            }
        }

        /// <summary>
        /// Set the Summary and Finance groups to its default state
        /// </summary>
        private void ClearSummaryAndFinance()
        {
            //Clear labels in Summary and Finance groups
            Label[] labels = { lblVehicleSalePrice, lblOptions, lblSubtotal, lblSalesTax,
                        lblTotal, lblTradeInAllowance, lblAmountDue, lblNoOfYears, lblInterestRate, lblMonthlyPayment };

            foreach (Label label in labels)
            {
                label.Text = string.Empty;
            }

            //Disable Finance group
            grpFinance.Enabled = false;
        }

        /// <summary>
        /// Calculates the SalesQuote
        /// </summary>
        private SalesQuote CalculateQuote()
        {
            //Get the index of the accessories chosen by the user (See: Accessories.cs)
            int accessoriesChosen = 0;
            CheckBox[] checkboxes = { chkStereoSystem, chkLeatherInterior, chkComputerNavigation };

            for(int i = 0; i < checkboxes.Length; i++)
            {
                //Check if the checkbox is checked
                if(checkboxes[i].Checked)
                {
                    //Since ComputerNavigation is in the 4th position in the enum, 4 will be added to "AccessoriesChosen".
                    if(i == 2)
                    {
                        accessoriesChosen += 4;
                    }
                    //Else, add the index of the checkbox to the accessoriesChosen
                    else
                    {
                        accessoriesChosen += (i + 1);
                    }
                }
            }

            //Get the index of the exteriopr finish chosen by the user (See: ExteriorFinish.cs)
            int exteriorFinishChosen = -1;
            RadioButton[] radiobuttons = { radStandard, radPearlized, radCustom };

            for(int i = 0; i < radiobuttons.Length || exteriorFinishChosen == -1; i++)
            {
                //Set the index of the exteriorFinish chosen by the user
                if(radiobuttons[i].Checked)
                {
                    exteriorFinishChosen += (i + 2);
                }
            }


            //Set the values of parameters necessary to construct a SalesQuote object
            decimal vehicleSalePrice = decimal.Parse(txtVehicleSalePrice.Text.Trim());
            decimal tradeInAllowance = decimal.Parse(txtTradeInAllowance.Text.Trim());
            decimal salesTaxRate = decimal.Parse(ConfigurationManager.AppSettings["GSTRate"]) +
                decimal.Parse(ConfigurationManager.AppSettings["PSTRate"]);
            Accessories accessories = (Accessories)accessoriesChosen;
            ExteriorFinish exteriorFinish = (ExteriorFinish)exteriorFinishChosen;

            //Return the SalesQuote
            return new SalesQuote(vehicleSalePrice, tradeInAllowance, salesTaxRate, accessories, exteriorFinish);
        }

        /// <summary>
        /// Updates the Summary group with the information from the SalesQuote object
        /// </summary>
        /// <param name="quote">A SalesQuote object that contains information about the sales quote that was calculated.</param>
        private void UpdateSummaryLabels(SalesQuote quote)
        {
            //Update labels in the Summary group
            lblVehicleSalePrice.Text = string.Format("{0:C}", quote.Subtotal - (quote.AccessoryCost + quote.FinishCost));
            lblOptions.Text = string.Format("{0:N}", quote.AccessoryCost + quote.FinishCost);
            lblSubtotal.Text = string.Format("{0:C}", quote.Subtotal);
            lblSalesTax.Text = string.Format("{0:N}", quote.SalesTax);
            lblTotal.Text = string.Format("{0:C}", quote.Total);
            lblTradeInAllowance.Text = string.Format("-{0:N}", quote.Total - quote.AmountDue);
            lblAmountDue.Text = string.Format("{0:C}", quote.AmountDue);
        }

        /// <summary>
        /// Update the Finance Group
        /// </summary>
        private void UpdateFinanceGroup(SalesQuote quote)
        {
            //Bind labels to the horizontal scrollbar values
            lblNoOfYears.Text = hsbNumberOfYears.Value.ToString();
            lblInterestRate.Text = string.Format("{0:F2}%", (decimal)hsbInterestRate.Value / 100);

            //Get the interest rate value
            decimal interestRate =  ((hsbInterestRate.Value / 100M)/100M)/12M;
            decimal paymentPeriods = hsbNumberOfYears.Value * 12;
            decimal presentValue = quote.AmountDue;

            //Calculate the monthly payment and display it
            lblMonthlyPayment.Text = string.Format("{0:C}", AutomotiveManager.Payment(interestRate, paymentPeriods, presentValue));
        }
        #endregion 
    }
}