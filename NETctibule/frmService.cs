﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//added
using System.Configuration;
using BusinessTier;

namespace NETctibule
{
    public partial class frmService : Form
    {
        //Add a class reference to a ServiceInvoice object
        private ServiceInvoice _invoice;

        public frmService()
        {
            InitializeComponent();

            //Add Event Listener (and Handler) for the load event of this form.
            this.Load += FrmService_Load;

            //Add Event Listener for focus event and validation events of textboxes
            TextBox[] textboxes = { txtDescription, txtCost };

            foreach (TextBox textbox in textboxes)
            {
                textbox.GotFocus += Textbox_GotFocus;
                textbox.Validating += Control_Validating;
                textbox.Validated += Control_Validated;
            }

            //Add Event Listener for Validating and Validated events of cboType
            cboType.Validating += Control_Validating;
            cboType.Validated += Control_Validated;

            //Event Listener for click event of btnAdd
            btnAdd.Click += BtnAdd_Click;

            //Event Listener for Generate Invoice menu item
            mnuServiceGenerateInvoice.Click += MnuServiceGenerateInvoice_Click;
        }

        #region EventHandler

        /// <summary>
        /// Event Handler for the load event of this form
        /// </summary>
        private void FrmService_Load(object sender, EventArgs e)
        {
            SetFormToDefault();
        }

        /// <summary>
        /// Event Handler for the click event of Generate Invoice menu item
        /// </summary>
        private void MnuServiceGenerateInvoice_Click(object sender, EventArgs e)
        {
            //Open an instance of Service Invoice
            frmServiceInvoice serviceInvoice = new frmServiceInvoice(_invoice);
            serviceInvoice.MdiParent = this.MdiParent;
            serviceInvoice.Show();

            //Reset the form
            SetFormToDefault();
        }

        /// <summary>
        /// Event Handler for the click event of btnAdd
        /// </summary>
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            if(this.ValidateChildren())
            {
                //Add costs to the Service Invoice
                _invoice.AddCost((CostType)cboType.SelectedIndex, decimal.Parse(txtCost.Text.Trim()));

                //Add the information to the DataGridView
                dgvServiceItems.Rows.Add(dgvServiceItems.RowCount + 1, txtDescription.Text.Trim(), cboType.SelectedItem, string.Format("${0}", txtCost.Text.Trim()));

                //Clear the input area
                SetInputAreaToDefault();

                //Update Summary labels
                lblSubtotal.Text = _invoice.Subtotal.ToString("C");
                lblPst.Text = _invoice.PSTCharged.ToString("C");
                lblGst.Text = _invoice.GSTCharged.ToString("C");
                lblTotal.Text = _invoice.Total.ToString("C");

                //Enable the mnuServiceGenerateInvoice
                mnuServiceGenerateInvoice.Enabled = true;

            }
        }

        /// <summary>
        /// Event Handler for the Validated event of all controls that need validation.
        /// </summary>
        private void Control_Validated(object sender, EventArgs e)
        {
            errorProvider.SetError((Control)sender, string.Empty);
        }

        /// <summary>
        /// Event Handler for the Validating event of a textbox.
        /// </summary>
        private void Control_Validating(object sender, CancelEventArgs e)
        {
            string errorMessage = string.Empty;

            //Check if nothing has been selected in cboType
            if (((Control)sender) is ComboBox && cboType.SelectedIndex == -1)
            {
                errorMessage = "Please select a service type.";
            }
            //Do more validations if the sender is a textbox.
            else if((Control)sender is TextBox)
            {
                //Check if txtDescription is empty.
                if (((TextBox)sender).Text.Trim().Length == 0 && (TextBox)sender == txtDescription)
                {
                    errorMessage = "Please enter a description.";
                }
                //Check if the input in txtCost can be parsed to a numeric value.
                else if ((TextBox)sender == txtCost && !AutomotiveManager.IsNumeric(((TextBox)sender).Text.Trim()))
                {
                    errorMessage = "Please enter a numeric value.";
                }
                //Check if the input in txtCost is greater than 0.
                else if ((TextBox)sender == txtCost && !(decimal.Parse(txtCost.Text.Trim()) > 0))
                {
                    errorMessage = "Please enter a value greater than 0.";
                }
            }
            

            //Check if the error message is empty. If it's not, it means that there's an error and error provider must be displayed.
            if(errorMessage.Length != 0)
            {
                e.Cancel = true;
                errorProvider.SetError((Control)sender, errorMessage);
            }
        }

        /// <summary>
        /// Event Handler for the GotFocus event of a textbox.
        /// </summary>
        private void Textbox_GotFocus(object sender, EventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }
        #endregion

        #region Method

        /// <summary>
        /// Set the form to default, which includes the input area, DataGridView and reinstantiating the SalesQuote.
        /// </summary>
        private void SetFormToDefault()
        {
            //Set input area to default
            SetInputAreaToDefault();

            //Clear the Data Grid View
            dgvServiceItems.Rows.Clear();

            //Clear summary labels
            lblSubtotal.Text = string.Empty;
            lblPst.Text = string.Empty;
            lblGst.Text = string.Empty;
            lblTotal.Text = string.Empty;

            //Instantiate the ServiceInvoice object
            decimal gst = decimal.Parse(ConfigurationManager.AppSettings["GSTRate"]);
            decimal pst = decimal.Parse(ConfigurationManager.AppSettings["PSTRate"]);
            _invoice = new ServiceInvoice(gst, pst);
        }
        /// <summary>
        /// Sets this form to its default state
        /// </summary>
        private void SetInputAreaToDefault()
        {
            txtDescription.Text = string.Empty;
            cboType.SelectedIndex = -1;
            txtCost.Text = string.Empty;
            txtDescription.Focus();
            mnuServiceGenerateInvoice.Enabled = false;
        }
        #endregion
    }
}
