using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//added
using DataTier;
using BusinessTier;

namespace NETctibule
{
    public partial class frmVehicleStock : Form
    {
        private BindingSource _vehicleStockBindingSource;
        private VehicleStockData _vehicleStockData;

        public frmVehicleStock()
        {
            InitializeComponent();

            //Add Event Listener for the load event of this form
            this.Load += FrmVehicleStock_Load;

            //Add Event Listener for the click event of the "New Vehicle" menu item
            mnuFileNewVehicle.Click += MnuFileNewVehicle_Click;

            //Add Event Listener when there's a row has been right-clicked
            dgvVehicles.CellMouseDown += DgvVehicles_CellMouseDown;

            //Add Event Listener for the DoubleClick Event on the DataGridView
            dgvVehicles.DoubleClick += DgvVehicles_DoubleClick;

            //Add Event Listener for the Edit Vehicle menu item
            mnuEdit.Click += MnuEdit_Click;

            //Add Event Listener for mnuRemove
            mnuRemove.Click += MnuRemove_Click;

            //Form Closing Event of this form
            this.FormClosing += FrmVehicleStock_FormClosing;

            //Add context menu when the rows are addded to the DataGridView
            dgvVehicles.RowsAdded += DgvVehicles_RowsAdded;
        }

        /// <summary>
        /// Adds context menu to the row of the DatGridView
        /// </summary>
        private void DgvVehicles_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dgvVehicles.Rows[e.RowIndex].ContextMenuStrip = msContextMenu;
        }

        /// <summary>
        /// Closes connection to the database then closes this form
        /// </summary>
        private void FrmVehicleStock_FormClosing(object sender, FormClosingEventArgs e)
        {
            _vehicleStockData.Close();
        }

        /// <summary>
        /// Event Handler for the Click event of the mnuRemove
        /// </summary>
        private void MnuRemove_Click(object sender, EventArgs e)
        {
            DataGridViewRow currentRow = dgvVehicles.SelectedRows[0];
            string vehicle = string.Format("{0} {1} {2}", currentRow.Cells["ManufacturedYear"].Value.ToString(), currentRow.Cells["Make"].Value.ToString(), currentRow.Cells["Model"].Value.ToString());
            
            try
            {
                //Display a warning message
                string errorMessage = string.Format("Are you sure your want to remove {0}", vehicle);
                DialogResult result = MessageBox.Show(errorMessage, "Remove Vehicle", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);

                //Remove the vehicle if the user pressed Yes
                if (result == DialogResult.Yes)
                {
                    dgvVehicles.Rows.RemoveAt(currentRow.Index);
                    _vehicleStockData.Update();
                }
            }
            //Show an error message when trying to remove a vehicle
            catch (Exception ex)
            {
                //Display an error message if there's an error removing the row from the DataGridView.
                string errorMessage = String.Format("An error occured removing {0}", vehicle);
                MessageBox.Show(errorMessage, "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                //Record error to log file
                AutomotiveManager.RecordError(errorMessage, ex);
            }
        }

        /// <summary>
        /// Evemt Handler for the click event of mnuEdit
        /// </summary>
        private void MnuEdit_Click(object sender, EventArgs e)
        {
            OpenEditVehicleStockForm(EditMode.Update);
        }

        /// <summary>
        /// Event Handler for the double-click event on the DataGridView
        /// </summary>
        private void DgvVehicles_DoubleClick(object sender, EventArgs e)
        {
            OpenEditVehicleStockForm(EditMode.Update);
        }

        /// <summary>
        /// Event Handler for the right-clicking of a row
        /// </summary>
        private void DgvVehicles_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && e.RowIndex > 0)
            {
                //Select the active cell
                dgvVehicles.CurrentCell = dgvVehicles.Rows[e.RowIndex].Cells[1];
            }
        }

        /// <summary>
        /// Event Handler for the click event for the "New Vehicle" menu item
        /// </summary>
        private void MnuFileNewVehicle_Click(object sender, EventArgs e)
        {
            OpenEditVehicleStockForm(EditMode.New);
        }

        /// <summary>
        /// Event Handler for the load event of this form.
        /// </summary>
        private void FrmVehicleStock_Load(object sender, EventArgs e)
        {
            try
            {
                //Connect the form to the database
                string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Data\\AMDatabase.mdb";
                string tableName = "VehicleStock";
                string selectQuery = string.Format("SELECT ID, StockNumber, ManufacturedYear, Make, Model, Mileage, Automatic, Colour, BasePrice, SoldBy, OptionsPrice FROM {0}", tableName);
                _vehicleStockData = new VehicleStockData(connectionString, tableName, selectQuery);
                _vehicleStockBindingSource = new BindingSource();
                _vehicleStockBindingSource.DataSource = _vehicleStockData.GetAllRows();

                //Set the binding source of the DataGridView
                dgvVehicles.DataSource = _vehicleStockBindingSource;
                dgvVehicles.Columns["ID"].Visible = false;
                dgvVehicles.Columns["SoldBy"].Visible = false;
                dgvVehicles.Columns["OptionsPrice"].Visible = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "An error occurred fetching vehicle stock data.";
                MessageBox.Show(errorMessage, "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                //Record error
                AutomotiveManager.RecordError(errorMessage, ex);

                //Close the form without closing connection to the database
                this.Shown += FrmVehicleStock_Shown;
            }
        }
        /// <summary>
        /// Close the form without closing connection to the database when there's an error connecting to it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmVehicleStock_Shown(object sender, EventArgs e)
        {
            this.FormClosing -= FrmVehicleStock_FormClosing;
            this.Close();
        }

        #region Methods

        /// <summary>
        /// Opens a new instance of the frmEditVehicleStock
        /// </summary>
        /// <param name="editMode">Specifies the mode of operation that will be done on this form.</param>
        private void OpenEditVehicleStockForm(EditMode editMode)
        {
            frmEditVehicleStock editVehicleStock;

            if(editMode == EditMode.New)
            {
                editVehicleStock = new frmEditVehicleStock(_vehicleStockData, _vehicleStockBindingSource, EditMode.New);
            }
            else
            {
                editVehicleStock = new frmEditVehicleStock(_vehicleStockData, _vehicleStockBindingSource, EditMode.Update);
            }

            editVehicleStock.ShowDialog();
        }
        #endregion
    }
}
