using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//added
using DataTier;
using BusinessTier;

namespace NETctibule
{
    /// <summary>
    /// Acceptable Edit Modes for this form
    /// </summary>
    public enum EditMode
    {
        New,
        Update
    }

    public partial class frmEditVehicleStock : Form
    {
        private VehicleStockData _vehicleStockData;
        private BindingSource _vehicleStockBindingSource;
        private EditMode _editMode;

        public frmEditVehicleStock(VehicleStockData vehicleStockData, BindingSource vehicleBindingSource, EditMode editMode)
        {
            InitializeComponent();

            //Initialize class variables
            _vehicleStockData = vehicleStockData;
            _vehicleStockBindingSource = vehicleBindingSource;
            _editMode = editMode;

            //Event Listener for the load event of this form
            this.Load += FrmEditVehicleStock_Load;

            //Event Listener for the click event of btnSave
            btnSave.Click += BtnSave_Click;

            //Validation of controls
            TextBox[] textboxes = { txtStockNumber, txtYear, txtMake, txtModel, txtMileage, txtColour, txtBasePrice };

            foreach(TextBox textbox in textboxes)
            {
                textbox.Validating += Textbox_Validating;
                textbox.Validated += Textbox_Validated;
            }

            //Event Listener for Form Closing event of this form
            this.FormClosing += FrmEditVehicleStock_FormClosing;
        }

        /// <summary>
        /// Event Handler for the Form Closing event of this form
        /// </summary>
        private void FrmEditVehicleStock_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Apply changes to BindingSource
            _vehicleStockBindingSource.EndEdit();

            DataRowState currentRowState = ((DataRowView)_vehicleStockBindingSource.Current).Row.RowState;

            if (_editMode == EditMode.New || currentRowState == DataRowState.Modified)
            {
                DialogResult result = MessageBox.Show("Do you want to save changes?",
                    string.Format("{0} Stock", this.Text), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button3);

                switch (result)
                {
                    case DialogResult.Yes:
                        bool successfulSave = IsSuccessfulSave();
                        e.Cancel = !successfulSave;
                        break;
                    case DialogResult.No:
                        _vehicleStockData.GetAllRows().RejectChanges();
                        break;
                    case DialogResult.Cancel:
                        e.Cancel = true;
                        break;
                }
            }
        }

        /// <summary>
        /// Removes the error provider when there's no more errors
        /// </summary>
        private void Textbox_Validated(object sender, EventArgs e)
        {
            errorProvider.SetError((TextBox)sender, string.Empty);
        }

        /// <summary>
        /// Validates textboxes of this form
        /// </summary>
        private void Textbox_Validating(object sender, CancelEventArgs e)
        {
            string errorMessage = string.Empty;
            string textBoxInput = ((TextBox)sender).Text.Trim();

            //Checks if the textbox if the textbox is empty.
            if (textBoxInput.Equals(string.Empty))
            {
                errorMessage = "Please enter a value for this field.";
            }
            //Checks if the EditMode is New, the sender is txtStockNumber and if the StockNumber has duplicates
            else if (_editMode == EditMode.New && ((TextBox)sender) == txtStockNumber && _vehicleStockData.IsDuplicateStockNumber(textBoxInput))
            {
                errorMessage = "This stock number is used by another vehicle. Please enter another stock number.";
            }
            //Checks if the sender is txtYear, if the value is a numeric value and if it is a 4-digit number.
            else if ((TextBox)sender == txtYear && (!IsNumeric(textBoxInput) ||
                ((Decimal.Parse(textBoxInput) < 1000 || Decimal.Parse(textBoxInput) > 9999))))
            {
                errorMessage = "Please enter a valid four digit year. Eg. 1977";
            }
            //Checks if the sender is txtMileage and if the input is a numeric value. 
            else if (((TextBox)sender == txtMileage || (TextBox)sender == txtBasePrice) && !IsNumeric(textBoxInput))
            {
                errorMessage = "Please enter a numeric value for this field";
            }

            //Set an error message if the error message is not empty.
            if (!errorMessage.Equals(string.Empty))
            {
                //Cancel the Validation and set the error message.
                e.Cancel = true;
                errorProvider.SetError((TextBox)sender, errorMessage);
            }
        }

        /// <summary>
        /// Event Handler for the click event of this button
        /// </summary>
        private void BtnSave_Click(object sender, EventArgs e)
        {
            if(IsSuccessfulSave())
            {
                //Close the form without prompting the user
                this.FormClosing -= FrmEditVehicleStock_FormClosing;
                this.Close();
            }
        }

        /// <summary>
        /// Event Handler for the load event of this form
        /// </summary>
        private void FrmEditVehicleStock_Load(object sender, EventArgs e)
        {
            try
            {
                //Set the form to its intended default state depending on the mode of the operation of the form.
                if (_editMode == EditMode.New)
                {
                    this.Text = "New Vehicle";
                    radAutomatic.Checked = true;
                }
                else
                {
                    BindControls();
                    txtStockNumber.Enabled = false;
                }
            }
            //Catch any exception when trying to bind controls to the current vehicle
            catch (Exception ex)
            {
                string errorMessage = "Could not load record";
                MessageBox.Show(errorMessage, "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                AutomotiveManager.RecordError(errorMessage, ex);
            }
        }

        #region Methods

        /// <summary>
        /// Save changes made to an existing record or insert a new record, depending on the mode of operation
        /// </summary>
        private bool IsSuccessfulSave()
        {
            bool successfulSave = false;

            //Apply changes made to the BindingSource
            _vehicleStockBindingSource.EndEdit();

            //Validate controls
            if (this.ValidateChildren())
            {
                try
                {
                    //Insert a new record to the database when the mode of operation is new
                    if (_editMode == EditMode.New)
                    {
                        //Reference to the Vehicles table in the database
                        DataTable vehicles = _vehicleStockData.GetAllRows();

                        //Declare a new record
                        DataRow row = vehicles.NewRow();

                        //Set values of the attributes of the row
                        row["StockNumber"] = txtStockNumber.Text.Trim();
                        row["ManufacturedYear"] = txtYear.Text.Trim();
                        row["Make"] = txtMake.Text.Trim();
                        row["Model"] = txtModel.Text.Trim();
                        row["Mileage"] = txtMileage.Text.Trim();
                        row["Automatic"] = radAutomatic.Checked;
                        row["Colour"] = txtColour.Text.Trim();
                        row["BasePrice"] = txtBasePrice.Text.Trim();
                        row["SoldBy"] = "0";
                        row["OptionsPrice"] = "0";

                        //Add new row to the DataTable
                        vehicles.Rows.Add(row);
                    }

                    

                    //Update the Database
                    _vehicleStockData.Update();

                    //Indicate that the save is successful
                    successfulSave = true;
                }
                //Catch exceptions when updating or inserting record on the database
                catch (Exception ex)
                {
                    string errorMessage = "Error Saving to Database";
                    MessageBox.Show(errorMessage, "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AutomotiveManager.RecordError(errorMessage, ex);
                }
        }

            return successfulSave;
        }

        /// <summary>
        /// Determines if a value can be parsed to a number of not.
        /// </summary>
        /// <param name="stringToTest">the string value to be tested</param>
        private bool IsNumeric(string stringToTest)
        {
            decimal tryParseVariable = 0M;

            return Decimal.TryParse(stringToTest, out tryParseVariable);
        }

        /// <summary>
        /// Bind controls to display information about the selected vehicle
        /// </summary>
        private void BindControls()
        {
            txtStockNumber.DataBindings.Add("Text", _vehicleStockBindingSource, "StockNumber");
            txtYear.DataBindings.Add("Text", _vehicleStockBindingSource, "ManufacturedYear");
            txtMake.DataBindings.Add("Text", _vehicleStockBindingSource, "Make");
            txtModel.DataBindings.Add("Text", _vehicleStockBindingSource, "Model");
            radAutomatic.DataBindings.Add("Checked", _vehicleStockBindingSource, "Automatic");
            txtMileage.DataBindings.Add("Text", _vehicleStockBindingSource, "Mileage");
            txtColour.DataBindings.Add("Text", _vehicleStockBindingSource, "Colour");
            txtBasePrice.DataBindings.Add("Text", _vehicleStockBindingSource, "BasePrice");
        }
        #endregion
    }
}
