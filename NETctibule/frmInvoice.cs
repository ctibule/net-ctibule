﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//added
using System.Configuration;

namespace NETctibule
{
    public partial class frmInvoice : Form
    {
        public frmInvoice()
        {
            InitializeComponent();

            //Set the labels of this form
            lblCompanyName.Text = ConfigurationManager.AppSettings["CompanyName"];
            lblAddress.Text = ConfigurationManager.AppSettings["CompanyAddress"];
            lblCity.Text = string.Format("{0}, {1} {2}", ConfigurationManager.AppSettings["CompanyCity"], ConfigurationManager.AppSettings["CompanyProvince"],
                ConfigurationManager.AppSettings["CompanyPostal"]);
            lblPhone.Text = ConfigurationManager.AppSettings["CompanyPhone"];
            lblDate.Text = DateTime.Today.ToString("MM/dd/yyyy");

            //Add Event Listener for the Print menu item
            mnuFilePrint.Click += MnuFilePrint_Click;
        }
        /// <summary>
        /// Event Handler for the Print menu item
        /// </summary>
        private void MnuFilePrint_Click(object sender, EventArgs e)
        {
            printer.Print(this, Microsoft.VisualBasic.PowerPacks.Printing.PrintForm.PrintOption.ClientAreaOnly);
        }
    }
}
